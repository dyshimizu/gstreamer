/*
 * GStreamer gstreamer-litertinference
 * Copyright (C) 2024 Collabora Ltd.
 *
 * gstlitertinference.cpp
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/**
 * SECTION:element-litertinference
 * @short_description: Run LiteRT (Tensorflow Lite) inference model on video buffers
 *
 * This element can apply an LiteRT (Tensorflow Lite) to video buffers.
 * It attaches the tensor output to the buffer as a @ref GstTensorMeta.
 *
 * ## Example launch command:
 *
 * GST_DEBUG=ssdobjectdetector:5 \
 * gst-launch-1.0 filesrc location=bus.jpg ! \
 * jpegdec ! videoconvert ! litertinference execution-provider=cpu model-file=ssd_mobilenet_v1_coco.LiteRT !  \
 * ssdobjectdetector label-file=COCO_classes.txt  ! videoconvert ! imagefreeze ! autovideosink
 *
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gmodule.h>

#include "tensorflow/lite/c/c_api.h"

#include <gst/gst.h>
#include <gst/video/video.h>
#include "gstlitertinference.h"
#include "modelinfo.h"

/*
 * GstMlInputImageFormat:
 *
 * @GST_ML_INPUT_IMAGE_FORMAT_HWC Height Width Channel (a.k.a. interleaved) format
 * @GST_ML_INPUT_IMAGE_FORMAT_CHW Channel Height Width  (a.k.a. planar) format
 */
typedef enum
{
  GST_ML_INPUT_IMAGE_FORMAT_HWC,
  GST_ML_INPUT_IMAGE_FORMAT_CHW,
} GstMlInputImageFormat;

typedef enum
{
  GST_LITERT_EXECUTION_PROVIDER_CPU,
  GST_LITERT_EXECUTION_PROVIDER_EXTERNAL_DELEGATE,
  GST_LITERT_EXECUTION_PROVIDER_EDGE_TPU,
} GstLiteRTExecutionProvider;


#define DEFAULT_MODEL_FILE              ""
#define DEFAULT_INPUT_IMAGE_FORMAT      GST_ML_INPUT_IMAGE_FORMAT_HWC
#define DEFAULT_EXECUTION_PROVIDER      GST_LITERT_EXECUTION_PROVIDER_CPU
#define DEFAULT_INPUT_INDEX             0
#define DEFAULT_EXT_DELEGATE_LIB_PATH   ""
#define DEFAULT_THREADS                 0
#define DEFAULT_INPUT_TENSOR_SCALE               1.0
#define DEFAULT_INPUT_TENSOR_OFFSET              0.0


/*
 * GstLiteRTInference:
 *
 * @model_file model file
 * @execution_provider: LiteRT execution provider
 * @litert_client opaque pointer to LiteRT client
 * @litert_disabled true if inference is disabled
 * @video_info @ref GstVideoInfo of sink caps
 */
struct _GstLiteRTInference
{
  GstBaseTransform basetransform;
  gchar *model_file;
  GstLiteRTExecutionProvider execution_provider;
  gboolean ext_delegate_path_present;
  gboolean m_allow_fp16;
  gfloat m_inputTensorOffset;
  gfloat m_inputTensorScale;
  gfloat m_inferenceTime;
  gsize m_numberOfThreads;
  GstLiteRTExecutionProvider m_provider;
  gchar *m_external_delegate_path;
  gboolean m_fixedInputImageSize;
  gchar *m_vxdelegate;
  GstMlInputImageFormat m_inputImageFormat;
  GPtrArray *m_tensor_templates;
  LiteRTInterpreter *m_interpreter;
  LiteRTModel *m_model;
  gboolean litert_disabled;
  GstVideoInfo video_info;
  guint8 *m_dest;
  guint32 m_width;
  guint32 m_height;
};

GST_DEBUG_CATEGORY (litert_inference_debug);

#define GST_CAT_DEFAULT litert_inference_debug
GST_ELEMENT_REGISTER_DEFINE (litert_inference, "litertinference",
    GST_RANK_NONE, GST_TYPE_LITERT_INFERENCE);

/* GstLiteRTInference properties */
enum
{
  PROP_0,
  PROP_MODEL_FILE,
  PROP_INPUT_IMAGE_FORMAT,
  PROP_EXECUTION_PROVIDER,
  PROP_INPUT_INDEX,
  PROP_EXT_DELEGATE_LIB_PATH,
  PROP_THREADS,
  PROP_INPUT_TENSOR_OFFSET,
  PROP_INPUT_TENSOR_SCALE
};

#define VIDEO_CAPS GST_VIDEO_CAPS_MAKE ("{ RGB, RGBA, BGR, BGRA }")

static GstStaticPadTemplate gst_litert_inference_src_template =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (VIDEO_CAPS)
    );

static GstStaticPadTemplate gst_litert_inference_sink_template =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (VIDEO_CAPS)
    );

static gboolean gst_litert_inference_start (GstBaseTransform * trans);

static void gst_litert_inference_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec);
static void gst_litert_inference_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);
static void gst_litert_inference_finalize (GObject * object);
static GstFlowReturn gst_litert_inference_transform_ip (GstBaseTransform *
    trans, GstBuffer * buf);
static gboolean gst_litert_inference_process (GstBaseTransform * trans,
    GstBuffer * buf);
static GstCaps *gst_litert_inference_transform_caps (GstBaseTransform *
    trans, GstPadDirection direction, GstCaps * caps, GstCaps * filter_caps);
static gboolean
gst_litert_inference_set_caps (GstBaseTransform * trans, GstCaps * incaps,
    GstCaps * outcaps);

G_DEFINE_TYPE (GstLiteRTInference, gst_litert_inference,
    GST_TYPE_BASE_TRANSFORM);

GType gst_litert_execution_provider_get_type (void);
#define GST_TYPE_LITERT_EXECUTION_PROVIDER (gst_litert_execution_provider_get_type ())

GType gst_ml_model_input_image_format_get_type (void);
#define GST_TYPE_ML_MODEL_INPUT_IMAGE_FORMAT (gst_ml_model_input_image_format_get_type ())

#ifdef EXTERNAL_DELEGATE
#define DELEGATE_DESC \
    "External delegate execution provider (set the ext-delegate property)"
#else
#define DELEGATE_DESC \
    "External delegate execution provider disabled (uses CPU)"
#endif

GType
gst_litert_execution_provider_get_type (void)
{
  static GType litert_execution_type = 0;

  if (g_once_init_enter (&litert_execution_type)) {
    static GEnumValue execution_provider_types[] = {
      {GST_LITERT_EXECUTION_PROVIDER_CPU, "CPU execution provider", "cpu"},
      {GST_LITERT_EXECUTION_PROVIDER_EXTERNAL_DELEGATE, DELEGATE_DESC,
          "external-delegate"},
      {GST_LITERT_EXECUTION_PROVIDER_EDGE_TPU,
#ifdef EDGETPU
            "Coral EdgeTPU acceleration",
#else
            "Coral EdgeTPU acceleration disabled (uses CPU)",
#endif
          "coral"},
      {0, NULL, NULL},
    };

    GType temp = g_enum_register_static ("GstLiteRTExecutionProvider",
        execution_provider_types);

    g_once_init_leave (&litert_execution_type, temp);
  }

  return litert_execution_type;
}

GType
gst_ml_model_input_image_format_get_type (void)
{
  static GType ml_model_input_image_format = 0;

  if (g_once_init_enter (&ml_model_input_image_format)) {
    static GEnumValue ml_model_input_image_format_types[] = {
      {GST_ML_INPUT_IMAGE_FORMAT_HWC, "Height Width Channel (HWC) a.k.a. "
            "interleaved image data format", "hwc"},
      {GST_ML_INPUT_IMAGE_FORMAT_CHW,
          "Channel Height Width (CHW) a.k.a. planar image data format", "chw"},
      {0, NULL, NULL},
    };

    GType temp = g_enum_register_static ("GstMlInputImageFormat",
        ml_model_input_image_format_types);

    g_once_init_leave (&ml_model_input_image_format, temp);
  }

  return ml_model_input_image_format;
}

static void
gst_litert_inference_class_init (GstLiteRTInferenceClass * klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;
  GstElementClass *element_class = (GstElementClass *) klass;
  GstBaseTransformClass *basetransform_class = (GstBaseTransformClass *) klass;

  GST_DEBUG_CATEGORY_INIT (litert_inference_debug, "litertinference",
      0, "litert_inference");
  gobject_class->set_property = gst_litert_inference_set_property;
  gobject_class->get_property = gst_litert_inference_get_property;
  gobject_class->finalize = gst_litert_inference_finalize;

  /**
   * GstLiteRTInference:model-file
   *
   * LiteRT model file
   *
   * Since: 1.26
   */
  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_MODEL_FILE,
      g_param_spec_string ("model-file",
          "LiteRT model file", "LiteRT model file", DEFAULT_MODEL_FILE,
          (GParamFlags)
          (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  /**
   * GstLiteRTInference:input-image-format
   *
   * Model input image format
   *
   * Since: 1.26
   */
  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_INPUT_IMAGE_FORMAT,
      g_param_spec_enum ("input-image-format",
          "Input image format",
          "Input image format",
          GST_TYPE_ML_MODEL_INPUT_IMAGE_FORMAT,
          DEFAULT_INPUT_IMAGE_FORMAT, (GParamFlags)
          (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  /**
   * GstLiteRTInference:execution-provider
   *
   * Execution provider
   *
   * Since: 1.26
   */
  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_EXECUTION_PROVIDER,
      g_param_spec_enum ("execution-provider",
          "Execution provider",
          "LiteRT execution provider",
          GST_TYPE_LITERT_EXECUTION_PROVIDER,
          DEFAULT_EXECUTION_PROVIDER, (GParamFlags)
          (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  /**
   * GstLiteRTInference:ext-delegate:
   *
   * LiteRT External Delegate
   *
   * Since: 1.26
   */
  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_EXT_DELEGATE_LIB_PATH, g_param_spec_string ("ext-delegate",
          "External Delegate",
          "External Delegate library path for the NN inference",
          DEFAULT_EXT_DELEGATE_LIB_PATH, (GParamFlags)
          (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  /**
   * GstLiteRTInference:execution-provider
   *
   * Number of Threads
   *
   * Since: 1.26
   */
  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_THREADS,
      g_param_spec_int ("threads",
          "Number of Threads",
          "Set the number of threads to be used by the LiteRT inference (-1 for auto)",
          -1, G_MAXINT, DEFAULT_THREADS,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  /**
   * GstLiteRTInference:input-tensor-scale:
   *
   * Input tensor scale
   *
   * Since: 1.26
   */
  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_INPUT_TENSOR_SCALE,
      g_param_spec_float ("input-tensor-scale",
          "Input tensor standard value",
          "Standard value of the input",
          -G_MAXFLOAT, G_MAXFLOAT, DEFAULT_INPUT_TENSOR_SCALE,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  /**
   * GstLiteRTInference:input-tensor-offset:
   *
   * Input tensor offset
   *
   * Since: 1.26
   */
  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_INPUT_TENSOR_OFFSET,
      g_param_spec_float ("input-tensor-offset",
          "Input tensor offset",
          "Offset each tensor value by this value",
          -G_MAXFLOAT, G_MAXFLOAT, DEFAULT_INPUT_TENSOR_OFFSET,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  gst_element_class_set_static_metadata (element_class, "litertinference",
      "Filter/Effect/Video",
      "Apply neural network to video frames and create tensor output",
      "Denis Shimizu <denis.shimizu@collabora.com>, "
      "Aaron Boxer <aaron.boxer@collabora.com>,"
      "Daniel Morin <daniel.morin@collabora.com>");
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&gst_litert_inference_sink_template));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&gst_litert_inference_src_template));
  basetransform_class->transform_ip =
      GST_DEBUG_FUNCPTR (gst_litert_inference_transform_ip);
  basetransform_class->transform_caps =
      GST_DEBUG_FUNCPTR (gst_litert_inference_transform_caps);
  basetransform_class->set_caps =
      GST_DEBUG_FUNCPTR (gst_litert_inference_set_caps);
  basetransform_class->start = GST_DEBUG_FUNCPTR (gst_litert_inference_start);
}

static gboolean
gst_litert_inference_has_session (GstLiteRTInference * self)
{
  return self->m_interpreter != NULL;
}

static void
gst_litert_inference_init (GstLiteRTInference * self)
{
  self->m_inputTensorOffset = DEFAULT_INPUT_TENSOR_OFFSET;
  self->m_inputTensorScale = DEFAULT_INPUT_TENSOR_SCALE;
  self->m_numberOfThreads = DEFAULT_THREADS;
  self->m_provider = DEFAULT_EXECUTION_PROVIDER;
  self->m_external_delegate_path = NULL;
  self->m_fixedInputImageSize = TRUE;
  self->m_inputImageFormat = DEFAULT_INPUT_IMAGE_FORMAT;
  self->m_tensor_templates = g_ptr_array_new_with_free_func ((GDestroyNotify)
      gst_tensor_free);
  self->litert_disabled = TRUE;
  self->ext_delegate_path_present = FALSE;
}

static void
gst_litert_inference_finalize (GObject * object)
{
  GstLiteRTInference *self = GST_LITERT_INFERENCE (object);

  g_free (self->model_file);
  g_ptr_array_unref (self->m_tensor_templates);
  G_OBJECT_CLASS (gst_litert_inference_parent_class)->finalize (object);
}

static void
gst_litert_inference_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstLiteRTInference *self = GST_LITERT_INFERENCE (object);
  const gchar *filename;

  switch (prop_id) {
    case PROP_MODEL_FILE:
      filename = g_value_get_string (value);
      if (filename
          && g_file_test (filename,
              (GFileTest) (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR))) {
        if (self->model_file)
          g_free (self->model_file);
        self->model_file = g_strdup (filename);
        self->litert_disabled = FALSE;
      } else {
        GST_WARNING_OBJECT (self, "Model file '%s' not found!", filename);
      }
      break;
    case PROP_EXECUTION_PROVIDER:
      self->execution_provider =
          (GstLiteRTExecutionProvider) g_value_get_enum (value);
      break;
    case PROP_EXT_DELEGATE_LIB_PATH:
      filename = g_value_get_string (value);
      if (filename) {
        self->m_external_delegate_path = g_strdup (filename);
        self->ext_delegate_path_present = true;
      } else {
        GST_WARNING_OBJECT (self, "Ext Delegate library file '%s' not found!",
            filename);
      }
      break;
    case PROP_INPUT_IMAGE_FORMAT:
      self->m_inputImageFormat =
          (GstMlInputImageFormat) g_value_get_enum (value);
      break;
    case PROP_THREADS:
      self->m_numberOfThreads = g_value_get_int (value);
      break;
    case PROP_INPUT_TENSOR_SCALE:
      self->m_inputTensorScale = g_value_get_float (value);
      break;
    case PROP_INPUT_TENSOR_OFFSET:
      self->m_inputTensorOffset = g_value_get_float (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_litert_inference_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstLiteRTInference *self = GST_LITERT_INFERENCE (object);

  switch (prop_id) {
    case PROP_MODEL_FILE:
      g_value_set_string (value, self->model_file);
      break;
    case PROP_EXECUTION_PROVIDER:
      g_value_set_enum (value, self->execution_provider);
      break;
    case PROP_EXT_DELEGATE_LIB_PATH:
      g_value_set_string (value, self->m_external_delegate_path);
      break;
    case PROP_INPUT_IMAGE_FORMAT:
      g_value_set_enum (value, self->m_inputImageFormat);
      break;
    case PROP_THREADS:
      g_value_set_int (value, self->m_numberOfThreads);
      break;
    case PROP_INPUT_TENSOR_SCALE:
      g_value_set_float (value, self->m_inputTensorScale);
      break;
    case PROP_INPUT_TENSOR_OFFSET:
      g_value_set_float (value, self->m_inputTensorOffset);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static GstTensorDataType
gst_litert_convert_data_type (LiteRTType type)
{
  switch (type) {
    case kTfLiteFloat32:
      return GST_TENSOR_DATA_TYPE_FLOAT32;
    case kTfLiteInt32:
      return GST_TENSOR_DATA_TYPE_INT32;
    case kTfLiteUInt8:
      return GST_TENSOR_DATA_TYPE_UINT8;
    case kTfLiteInt64:
      return GST_TENSOR_DATA_TYPE_INT64;
    case kTfLiteInt16:
      return GST_TENSOR_DATA_TYPE_INT16;
    case kTfLiteInt8:
      return GST_TENSOR_DATA_TYPE_INT8;
    case kTfLiteFloat16:
      return GST_TENSOR_DATA_TYPE_FLOAT16;
    case kTfLiteFloat64:
      return GST_TENSOR_DATA_TYPE_FLOAT64;
    case kTfLiteUInt64:
      return GST_TENSOR_DATA_TYPE_UINT64;
    case kTfLiteUInt32:
      return GST_TENSOR_DATA_TYPE_UINT32;
    case kTfLiteUInt16:
      return GST_TENSOR_DATA_TYPE_UINT16;
    case kTfLiteInt4:
      return GST_TENSOR_DATA_TYPE_INT4;
#ifdef LITERT_HAS_BFLOAT16
    case kTfLiteBFloat16:
      return GST_TENSOR_DATA_TYPE_BFLOAT16;
#endif

    default:
      GST_FIXME ("GstTensorDataType currently does not have a mapping \
          for this type. Returns GST_TENSOR_DATA_TYPE_INT32 for now.");
      g_assert_not_reached ();
  }
}

static gboolean
gst_litert_inference_create_session (GstLiteRTInference * self,
    const gchar * model_file, GstLiteRTExecutionProvider provider)
{
  LiteRTModel *model;
  LiteRTInterpreter *interpreter;
  LiteRTInterpreterOptions *interpreter_option;
  GKeyFile *modelinfo;
  gchar *group_name = NULL;
  gsize o_size;

  self->m_allow_fp16 = FALSE;
  self->m_inferenceTime = 0;

  /* Create and configure interpreter options */
  interpreter_option = LiteRTInterpreterOptionsCreate ();
  if (self->m_numberOfThreads != 0) {
    LiteRTInterpreterOptionsSetNumThreads (interpreter_option,
        self->m_numberOfThreads);
  }

  /* Handle provider-specific settings */
  switch (provider) {
    case GST_LITERT_EXECUTION_PROVIDER_EXTERNAL_DELEGATE:
      typedef LiteRTDelegate *(*CreateDelegateFunc) (void);
      CreateDelegateFunc create_delegate = NULL;

      GST_DEBUG_OBJECT (self,
          "Attempting to load external delegate library: %s",
          self->m_external_delegate_path);
      GModule *module =
          g_module_open (self->m_external_delegate_path, G_MODULE_BIND_LAZY);
      if (!module) {
        GST_ERROR_OBJECT (self, "Failed to load external delegate library: %s",
            g_module_error ());
        return FALSE;
      }
      GST_DEBUG_OBJECT (self, "External delegate library loaded successfully.");

      GST_DEBUG_OBJECT (self,
          "Looking up symbol 'LiteRTExternalDelegateCreate' in external delegate library.");
      if (!g_module_symbol (module, "LiteRTExternalDelegateCreate",
              (gpointer *) & create_delegate)) {
        GST_ERROR_OBJECT (self, "Failed to find delegate creation symbol in %s",
            self->m_external_delegate_path);
        g_module_close (module);
        return FALSE;
      }
      GST_DEBUG_OBJECT (self,
          "Symbol 'LiteRTExternalDelegateCreate' found, calling create_delegate().");
      LiteRTDelegate *ext_delegate = create_delegate ();
      if (!ext_delegate) {
        GST_ERROR_OBJECT (self, "Failed to create external delegate");
        g_module_close (module);
        return FALSE;
      }
      GST_DEBUG_OBJECT (self,
          "External delegate created successfully. Adding delegate to interpreter options.");
      LiteRTInterpreterOptionsAddDelegate (interpreter_option, ext_delegate);
      GST_DEBUG_OBJECT (self,
          "Delegate successfully added to interpreter options.");
      break;

    case GST_LITERT_EXECUTION_PROVIDER_EDGE_TPU:
      GST_WARNING_OBJECT (self, "Edge TPU not supported");
      break;

    case GST_LITERT_EXECUTION_PROVIDER_CPU:
    default:
      GST_DEBUG_OBJECT (self,
          "Using CPU execution provider, no delegate added.");
      break;
  }

  self->m_provider = provider;
  if (model_file == NULL) {
    GST_ERROR_OBJECT (self, "no model file name");
    return false;
  }

  model = LiteRTModelCreateFromFile (model_file);
  if (!model) {
    GST_ERROR_OBJECT (self, "Failed to mmap model %s", model_file);
    return false;
  }

  GST_DEBUG_OBJECT (self, "Loaded model %s", model_file);

  interpreter = LiteRTInterpreterCreate (model, interpreter_option);
  if (!interpreter) {
    GST_ERROR_OBJECT (self, "Failed to construct interpreter");
    return false;
  }

  modelinfo = modelinfo_load (model_file);
  if (!modelinfo) {
    GST_ERROR_OBJECT (self, "Can't find modelinfo for %s", model_file);
    return false;
  }

  o_size = LiteRTInterpreterGetOutputTensorCount (interpreter);
  for (guint i = 0; i < o_size; i++) {
    const LiteRTTensor *litert_tensor =
        LiteRTInterpreterGetOutputTensor (interpreter, i);

    gsize dims_count = LiteRTTensorNumDims (litert_tensor);
    gsize *dims = (gsize *) g_malloc0_n (dims_count, sizeof (gsize));

    for (gsize j = 0; j < dims_count; j++)
      dims[j] = LiteRTTensorDim (litert_tensor, j);

    GstTensorDataType data_type =
        gst_litert_convert_data_type (LiteRTTensorType (litert_tensor));

    const gchar *tname = LiteRTTensorName (litert_tensor);
    if (tname)
      group_name = modelinfo_find_group_name_by_name (modelinfo, tname);
    if (group_name) {
      if (!modelinfo_validate (modelinfo, group_name, data_type,
              dims_count, dims)) {
        g_free (group_name);
        group_name = NULL;
      }
    }

    if (!group_name)
      group_name = modelinfo_find_group_name_by_index (modelinfo, i);

    if (group_name) {
      if (!modelinfo_validate (modelinfo, group_name, data_type, dims_count,
              dims)) {
        g_free (group_name);
        group_name = NULL;
      }
    }

    if (group_name == NULL) {
      GST_ERROR_OBJECT (self,
          "Model info file doesn't contain info for tensor[%u]: %s matching the"
          " dimensions ", i, tname);
      g_ptr_array_set_size (self->m_tensor_templates, 0);
      return false;
    }

    GstTensor *t = gst_tensor_alloc (dims_count);

    t->id = modelinfo_get_quark_id (modelinfo, group_name);
    t->layout = GST_TENSOR_LAYOUT_CONTIGUOUS;
    t->data_type = data_type;
    t->dims_order = GST_TENSOR_DIM_ORDER_ROW_MAJOR;
    memcpy (t->dims, dims, sizeof (gsize) * t->num_dims);

    g_ptr_array_add (self->m_tensor_templates, t);

    g_free (group_name);
  }

  LiteRTTensor *itensor = LiteRTInterpreterGetInputTensor (interpreter, 0);
  if (LiteRTTensorType (itensor) == kLiteRTFloat32) {
    GST_DEBUG_OBJECT (self, "Floating point LiteRT Model");
  }

  self->m_interpreter = interpreter;
  self->m_model = model;

  return true;

}

static gboolean
gst_litert_inference_start (GstBaseTransform * trans)
{
  GstLiteRTInference *self = GST_LITERT_INFERENCE (trans);
  gboolean ret = FALSE;

  GST_OBJECT_LOCK (self);
  if (gst_litert_inference_has_session (self)) {
    ret = TRUE;
    goto done;
  }

  if (self->model_file == NULL) {
    GST_ELEMENT_ERROR (self, STREAM, FAILED, (NULL),
        ("model-file property not set"));
    goto done;
  }

  if ((self->ext_delegate_path_present == false) &&
      (self->execution_provider ==
          GST_LITERT_EXECUTION_PROVIDER_EXTERNAL_DELEGATE)) {
    GST_ELEMENT_ERROR (self, STREAM, FAILED, (NULL),
        ("ext-delegate property not set"));
    goto done;
  }

  ret = gst_litert_inference_create_session (self, self->model_file,
      self->execution_provider);

  if (!ret) {
    GST_ERROR_OBJECT (self,
        "Unable to create LiteRT session. Inference is disabled.");
  }

done:
  GST_OBJECT_UNLOCK (self);

  return ret;
}

static gboolean
_get_input_params (GstBaseTransform * trans, GstTensorDataType * datatype,
    gint32 * width, gint32 * height, gint32 * channels_count)
{
  GstLiteRTInference *self = GST_LITERT_INFERENCE (trans);
  LiteRTTensor *inputs = LiteRTInterpreterGetInputTensor (self->m_interpreter,
      0);

  g_return_val_if_fail (inputs != NULL, FALSE);

  if (datatype != NULL) {
    LiteRTType tfdatatype = LiteRTTensorType (inputs);
    *datatype = gst_litert_convert_data_type (tfdatatype);
  }

  if (width != NULL) {
    *width = LiteRTTensorDim (inputs, 2);
  }

  if (height != NULL) {
    *height = LiteRTTensorDim (inputs, 1);
  }

  if (channels_count) {
    *channels_count = LiteRTTensorDim (inputs, 3);
  }

  return TRUE;
}

static GstCaps *
gst_litert_inference_transform_caps (GstBaseTransform * trans,
    GstPadDirection direction, GstCaps * caps, GstCaps * filter_caps)
{
  GstLiteRTInference *self = GST_LITERT_INFERENCE (trans);
  GstCaps *other_caps;
  GstCaps *restrictions;
  GstTensorDataType datatype;
  gint width, height, channels;

  if (!gst_litert_inference_has_session (self)) {
    other_caps = gst_caps_ref (caps);
    goto done;
  }

  if (!_get_input_params (trans, &datatype, &width, &height, &channels)) {
    GST_ERROR_OBJECT (self, "Failed to get inputs parameters");
    return NULL;
  }

  restrictions = gst_caps_new_empty_simple ("video/x-raw");
  if (self->m_fixedInputImageSize)
    gst_caps_set_simple (restrictions, "width", G_TYPE_INT, width, "height",
        G_TYPE_INT, height, NULL);


  if (datatype == GST_TENSOR_DATA_TYPE_UINT8 &&
      self->m_inputTensorOffset == 1.0 && self->m_inputTensorScale == 0.0) {

    switch (channels) {
      case 1:
        gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "GRAY8",
            NULL);
        break;
      case 3:
        switch (self->m_inputImageFormat) {
          case GST_ML_INPUT_IMAGE_FORMAT_HWC:
            gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "RGB",
                NULL);
            break;
          case GST_ML_INPUT_IMAGE_FORMAT_CHW:
            gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "RGBP",
                NULL);
            break;
        }
        break;
      case 4:
        switch (self->m_inputImageFormat) {
          case GST_ML_INPUT_IMAGE_FORMAT_HWC:
            gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "RGBA",
                NULL);
            break;
          case GST_ML_INPUT_IMAGE_FORMAT_CHW:
            gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "RGBAP",
                NULL);
            break;
        }
        break;
      default:
        GST_ERROR_OBJECT (self, "Invalid number of channels %d", channels);
        return NULL;
    }
  }

  GST_DEBUG_OBJECT (self, "Applying caps restrictions: %" GST_PTR_FORMAT,
      restrictions);

  other_caps = gst_caps_intersect_full (caps, restrictions,
      GST_CAPS_INTERSECT_FIRST);
  gst_caps_unref (restrictions);

done:
  if (filter_caps) {
    GstCaps *tmp = gst_caps_intersect_full (other_caps, filter_caps,
        GST_CAPS_INTERSECT_FIRST);
    gst_caps_replace (&other_caps, tmp);
    gst_caps_unref (tmp);
  }

  return other_caps;
}

static gboolean
gst_litert_inference_set_caps (GstBaseTransform * trans, GstCaps * incaps,
    GstCaps * outcaps)
{
  GstLiteRTInference *self = GST_LITERT_INFERENCE (trans);

  if (!gst_video_info_from_caps (&self->video_info, incaps)) {
    GST_ERROR_OBJECT (self, "Failed to parse caps");
    return FALSE;
  }

  if (self->m_fixedInputImageSize) {
    self->m_width = self->video_info.width;
    self->m_height = self->video_info.height;
  } else {
    GST_WARNING_OBJECT (self, "Allocating before knowing model input size");
  }

  return TRUE;
}

static GstFlowReturn
gst_litert_inference_transform_ip (GstBaseTransform * trans, GstBuffer * buf)
{
  if (!gst_base_transform_is_passthrough (trans)
      && !gst_litert_inference_process (trans, buf)) {
    GST_ELEMENT_ERROR (trans, STREAM, FAILED,
        (NULL), ("LiteRT inference failed"));
    return GST_FLOW_ERROR;
  }

  return GST_FLOW_OK;
}

#define _convert_image_remove_alpha(Type, dst, hwc, srcPtr,                   \
    srcSamplesPerPixel, stride, offset, div)                                  \
G_STMT_START {                                                                \
  size_t destIndex = 0;                                                       \
  Type tmp;                                                                   \
                                                                              \
  if (self->m_inputImageFormat == GST_ML_INPUT_IMAGE_FORMAT_HWC) {            \
    for (int32_t j = 0; j < dstHeight; ++j) {                                 \
      for (int32_t i = 0; i < dstWidth; ++i) {                                \
        for (int32_t k = 0; k < dstChannels; ++k) {                           \
          tmp = *srcPtr[k];                                                   \
          tmp += offset;                                                      \
          dst[destIndex++] = (Type)(tmp / div);                               \
          srcPtr[k] += srcSamplesPerPixel;                                    \
        }                                                                     \
      }                                                                       \
      /* correct for stride */                                                \
      for (uint32_t k = 0; k < 3; ++k)                                        \
        srcPtr[k] += stride - srcSamplesPerPixel * dstWidth;                  \
    }                                                                         \
  } else {                                                                    \
    size_t frameSize = dstWidth * dstHeight;                                  \
    Type *destPtr[3] = { dst, dst + frameSize, dst + 2 * frameSize };         \
    for (int32_t j = 0; j < dstHeight; ++j) {                                 \
      for (int32_t i = 0; i < dstWidth; ++i) {                                \
        for (int32_t k = 0; k < dstChannels; ++k) {                           \
          tmp = *srcPtr[k];                                                   \
          tmp += offset;                                                      \
          destPtr[k][destIndex] = (Type)(tmp / div);                          \
          srcPtr[k] += srcSamplesPerPixel;                                    \
        }                                                                     \
        destIndex++;                                                          \
      }                                                                       \
      /* correct for stride */                                                \
      for (uint32_t k = 0; k < 3; ++k)                                        \
        srcPtr[k] += stride - srcSamplesPerPixel * dstWidth;                  \
    }                                                                         \
  }                                                                           \
}                                                                             \
G_STMT_END;

static void
gst_litert_inference_convert_image_remove_alpha_u8 (GstLiteRTInference * self,
    guint8 * dst, gint dstWidth, gint dstHeight, gint dstChannels,
    GstMlInputImageFormat hwc, guint8 ** srcPtr, guint8 srcSamplesPerPixel,
    guint32 stride, guint8 offset, guint8 div)
{
  _convert_image_remove_alpha (guint8, dst, hwc, srcPtr, srcSamplesPerPixel,
      stride, offset, div);
}

static void
gst_litert_inference_convert_image_remove_alpha_f32 (GstLiteRTInference * self,
    gfloat * dst, gint dstWidth, gint dstHeight, gint dstChannels,
    GstMlInputImageFormat hwc, guint8 ** srcPtr, guint8 srcSamplesPerPixel,
    guint32 stride, guint8 offset, guint8 div)
{
  _convert_image_remove_alpha (gfloat, dst, hwc, srcPtr, srcSamplesPerPixel,
      stride, offset, div);
}

static gboolean
gst_litert_inference_process (GstBaseTransform * trans, GstBuffer * buf)
{
  GstLiteRTInference *self = GST_LITERT_INFERENCE (trans);
  GstMapInfo info;
  guint8 *srcPtr[3];
  gsize srcSamplesPerPixel = 3;
  GstTensorDataType datatype;

  if (gst_buffer_map (buf, &info, GST_MAP_READ)) {

    // <==
    srcPtr[0] = info.data;
    srcPtr[1] = info.data + 1;
    srcPtr[2] = info.data + 2;

    switch (self->video_info.finfo->format) {
      case GST_VIDEO_FORMAT_RGBA:
        srcSamplesPerPixel = 4;
        break;
      case GST_VIDEO_FORMAT_BGRA:
        srcSamplesPerPixel = 4;
        srcPtr[0] = info.data + 2;
        srcPtr[1] = info.data + 1;
        srcPtr[2] = info.data + 0;
        break;
      case GST_VIDEO_FORMAT_ARGB:
        srcSamplesPerPixel = 4;
        srcPtr[0] = info.data + 1;
        srcPtr[1] = info.data + 2;
        srcPtr[2] = info.data + 3;
        break;
      case GST_VIDEO_FORMAT_ABGR:
        srcSamplesPerPixel = 4;
        srcPtr[0] = info.data + 3;
        srcPtr[1] = info.data + 2;
        srcPtr[2] = info.data + 1;
        break;
      case GST_VIDEO_FORMAT_BGR:
        srcPtr[0] = info.data + 2;
        srcPtr[1] = info.data + 1;
        srcPtr[2] = info.data + 0;
        break;
      default:
        break;
    }

    if (LiteRTInterpreterAllocateTensors (self->m_interpreter) != kLiteRTOk) {
      GST_ERROR_OBJECT (self, "Failed to allocate tensors");
      return FALSE;
    }

    LiteRTTensor *tensor = LiteRTInterpreterGetInputTensor (self->m_interpreter,
        0);

    gint width = LiteRTTensorDim (tensor, 2);
    gint height = LiteRTTensorDim (tensor, 1);
    gint channels = LiteRTTensorDim (tensor, 3);
    guint32 stride = self->video_info.stride[0];

    datatype = gst_litert_convert_data_type (LiteRTTensorType (tensor));
    switch (datatype) {
      case GST_TENSOR_DATA_TYPE_UINT8:{
        uint8_t *dest = (uint8_t *) LiteRTTensorData (tensor);

        if (dest == NULL)
          return false;
        gst_litert_inference_convert_image_remove_alpha_u8 (self,
            dest, width, height, channels, self->m_inputImageFormat, srcPtr,
            srcSamplesPerPixel, stride, (uint8_t) self->m_inputTensorScale,
            (uint8_t) 1);
        break;
      }
      case GST_TENSOR_DATA_TYPE_FLOAT32:{
        float *dest = (float *) LiteRTTensorData (tensor);

        if (dest == NULL)
          return false;
        gst_litert_inference_convert_image_remove_alpha_f32 (self, dest,
            width, height, channels, self->m_inputImageFormat, srcPtr,
            srcSamplesPerPixel, stride, self->m_inputTensorOffset,
            self->m_inputTensorScale);
        break;
      }
      default:{
        GST_ERROR_OBJECT (self, "Data type not handled");
        return false;
      }
        break;
    }

    /* Run inference */
    if (LiteRTInterpreterInvoke (self->m_interpreter) != kLiteRTOk) {
      GST_ERROR_OBJECT (self, "Failed to invoke litert!");
      return false;
    }

    gsize num_tensors =
        LiteRTInterpreterGetOutputTensorCount (self->m_interpreter);

    g_assert (num_tensors == self->m_tensor_templates->len);
    GstTensor **tensors =
        (GstTensor **) g_malloc0_n (num_tensors, sizeof (gpointer));

    for (size_t i = 0; i < num_tensors; i++) {

      const LiteRTTensor *output_tensor =
          LiteRTInterpreterGetOutputTensor (self->m_interpreter, i);

      tensors[i] = gst_tensor_alloc (LiteRTTensorNumDims (output_tensor));
      memcpy (tensors[i], g_ptr_array_index (self->m_tensor_templates, i),
          sizeof (GstTensor));
      tensors[i]->num_dims = LiteRTTensorNumDims (output_tensor);

      for (gsize j = 0; j < tensors[i]->num_dims; j++)
        tensors[i]->dims[j] = LiteRTTensorDim (output_tensor, j);;

      tensors[i]->data =
          gst_buffer_new_allocate (NULL, LiteRTTensorByteSize (output_tensor),
          NULL);

      gst_buffer_fill (tensors[i]->data, 0, LiteRTTensorData (output_tensor),
          LiteRTTensorByteSize (output_tensor));
    }

    GstTensorMeta *tmeta = gst_buffer_add_tensor_meta (buf);
    gst_tensor_meta_set (tmeta, num_tensors, tensors);

    if (!tmeta)
      return FALSE;

    GST_TRACE_OBJECT (trans, "Num tensors: %zu", tmeta->num_tensors);
    gst_buffer_unmap (buf, &info);
  }

  return TRUE;
}
