

#include <glib.h>
#include <gst/analytics/analytics.h>

#pragma once

G_BEGIN_DECLS

GKeyFile *
modelinfo_load (const gchar *model_filename);

gchar *
modelinfo_get_id (GKeyFile *modelinfo, const gchar * group_name);

GQuark
modelinfo_get_quark_id (GKeyFile *modelinfo, const gchar * group_name);

gboolean
modelinfo_validate (GKeyFile *modelinfo, const gchar * group_name,
    GstTensorDataType data_type, gsize num_dims, const gsize * dims);

gchar *
modelinfo_find_group_name_by_name (GKeyFile *modelinfo, const char *name);

gchar *
modelinfo_find_group_name_by_index (GKeyFile *modelinfo, gsize index);

G_END_DECLS
