
#include "modelinfo.h"

static gboolean
key_file_string_matches (GKeyFile * keyfile, const gchar * group,
    const gchar * key, const gchar * value)
{
  gchar *kf_value = g_key_file_get_string (keyfile, group, key, NULL);

  gboolean matches = !g_strcmp0 (kf_value, value);

  g_free (kf_value);

  return matches;
}

gchar *
modelinfo_get_id (GKeyFile * modelinfo, const gchar * group_name)
{
  return g_key_file_get_string (modelinfo, group_name, "id", NULL);
}

GQuark
modelinfo_get_quark_id (GKeyFile * modelinfo, const gchar * group_name)
{
  GQuark q = 0;
  gchar *id = g_key_file_get_string (modelinfo, group_name, "id", NULL);

  if (id)
    q = g_quark_from_string (id);
  g_free (id);

  return q;
}

gboolean
modelinfo_validate (GKeyFile * modelinfo, const gchar * group_name,
    GstTensorDataType data_type, gsize num_dims, const gsize * dims)
{
  gsize kf_dims_length = 0;
  gint *kf_dims;
  gsize i;
  gboolean ret = FALSE;

  if (!key_file_string_matches (modelinfo, group_name, "type",
          gst_tensor_data_type_get_name (data_type)))
    return FALSE;

  kf_dims = g_key_file_get_integer_list (modelinfo, group_name, "dims",
      &kf_dims_length, NULL);
  if (kf_dims == NULL)
    return TRUE;
  if (kf_dims_length != num_dims)
    goto done;

  for (i = 0; i < kf_dims_length; i++) {
    if (kf_dims[i] != dims[i])
      goto done;
  }

  ret = TRUE;
done:
  g_free (kf_dims);
  return ret;
}

gchar *
modelinfo_find_group_name_by_name (GKeyFile * modelinfo, const char *name)
{
  gchar **groups;
  gsize i;
  gchar *group_name = NULL;

  groups = g_key_file_get_groups (modelinfo, NULL);

  for (i = 0; groups[i]; i++) {
    if (!g_str_has_prefix (groups[i], "output-"))
      continue;

    if (key_file_string_matches (modelinfo, groups[i], "name", name)) {
      group_name = g_strdup (groups[i]);
      break;
    }
  }

  g_strfreev (groups);
  return group_name;
}

gchar *
modelinfo_find_group_name_by_index (GKeyFile * modelinfo, gsize index)
{
  gchar *group_name = g_strdup_printf ("output-%zu", index);

  if (!g_key_file_has_group (modelinfo, group_name)) {
    g_free (group_name);
    group_name = NULL;
  }

  return group_name;
}

GKeyFile *
modelinfo_load (const gchar * model_filename)
{
  GKeyFile *modelinfo = g_key_file_new ();
  gchar *filename;
  gboolean ret;
  gchar *last_dot;

  filename = g_strconcat (model_filename, ".modelinfo", NULL);
  ret = g_key_file_load_from_file (modelinfo, filename, G_KEY_FILE_NONE, NULL);
  g_free (filename);
  if (ret)
    return modelinfo;

  last_dot = g_utf8_strrchr (model_filename, -1, '.');
  if (last_dot && !g_utf8_strchr (last_dot, -1, '/')) {
    gchar *tmp = g_strndup (model_filename, last_dot - model_filename);
    filename = g_strconcat (tmp, ".modelinfo", NULL);
    g_free (tmp);
    ret =
        g_key_file_load_from_file (modelinfo, filename, G_KEY_FILE_NONE, NULL);
    g_free (filename);
    if (ret)
      return modelinfo;
  }

  g_key_file_free (modelinfo);
  return NULL;
}
